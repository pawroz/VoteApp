# VoteApp
> Application for the employees from Aviva Ubezpieczenia - II Oddział w Poznaniu.
> The application is designed to help make decisions about date of meetings.

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Features](#features)
* [Status](#status)
* [Contact](#contact)
* [Screenshots](#screenshots)

## General info



## Technologies
* Django
* PostgreSQL
* Bootstrap
* JavaScript

## Setup
You need to have python3 installed. In project folder: 
* `pip3 install -r requirements.txt`
* `python manage.py runserver`



## Features
* Authorization - login, registration
* Reset password sending for email
* Voting
* Creating polls
* Realtime charts created with

## Status
Project is: _frozen_


## Screenshots
![Example screenshot](./VoteProject/static/forReadMe/Aviva_Pzn_Północ1.png)


![Example screenshot](./VoteProject/static/forReadMe/Aviva_Pzn_Północ3.png)


![Example screenshot](./VoteProject/static/forReadMe/Aviva_Pzn_Północ4.png)


![Example screenshot](./VoteProject/static/forReadMe/Aviva_Pzn_Północ.png)
